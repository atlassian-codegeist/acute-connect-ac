# ACUTE #



### What is ACUTE? ###

**ACUTE (Atlassian Connect Utility & Technical Ecosystem)** is a collection of  **Atlassian Connect** cloud utilities and apps featuring **Jira**, **Bitbucket**, **Confluence**, and **Trello**.

These apps include:

* **Jira-On-Helium** is an integration of **Jira** and the **Helium Network**.

* **Confluence-On-Helium** is an integration of **Confluence** and the **Helium Network**.

* **BucketList** is a list of automated add-ons and upgrades to **BitBucket**.

* **AirTrello** is an integration of **Airtable** and **Trello**.

